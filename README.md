This document has been prepared by Osman Turgut only for the UI case study installation and information.

Browsers
=====
Browser parameter can be valued as chrome and firefox
If you set headlessmode = true, the browser will work with headlessMode.
 
Usage
=====

mvn clean test -Dbrowser=chrome -DheadlessMode=false
