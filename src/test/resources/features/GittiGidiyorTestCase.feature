Feature: GittiGidiyor UI case study

  Scenario: The process has been succesfully completed
    Given User go to gittigidiyor page
    Then User is on homepage
    When User click girisYap button
    And User fills "osmntrgt23@hotmail.com" to username textbox
    And User fills "123osman" to password textbox
    When User click login button
    Then User succesfully logined
    When User search "bilgisayar" in search textbox
    When User click bul button
    When User click secound page
    Then User check secound page opened
    When User select random product
    When User click add to basket button
    Then User check product price with after added basket of product price
    When User click increase button
    Then User go to basket and delete product