package StepDefinitions;

import Constants.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Hooks implements Constants {

	public static WebDriver driver;

	@Before
	public void openBrowser() throws Exception {

		String browserName = getParameter("browser");
		String headlessModeOnOff = getParameter("headlessMode");

		if (browserName.equals(CHROME)) {
			setChromeDriver(headlessModeOnOff);
		} else if (browserName.equals(FIREFOX)) {
			setFirefoxDriver(headlessModeOnOff);
		}
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.get(URL);
	}

	private String getParameter(String name) {
		String value = System.getProperty(name);

		if (value == null) {
			throw new RuntimeException(name + " is not a parameter!");
		}

		if (value.isEmpty()) {
			throw new RuntimeException(name + " is empty!");
		}

		return value;
	}

	@After
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (WebDriverException noSupportScreenshot) {
				System.err.println(noSupportScreenshot.getMessage());
			}
		}
		driver.quit();
	}

	private void setChromeDriver(String headlessModeOnOff) {

		WebDriverManager.chromedriver().setup();

		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--start-maximized");

		if (headlessModeOnOff.equals("true")) {
			chromeOptions.addArguments("--headless");
			chromeOptions.addArguments("--disable-gpu");
			chromeOptions.addArguments("window-size=1920,1080");
		}
		else {
			driver = new ChromeDriver(chromeOptions);
		}
	}

	private void setFirefoxDriver(String headlessModeOnOff) {
		WebDriverManager.firefoxdriver().setup();
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		FirefoxBinary firefoxBinary = new FirefoxBinary();

		if (headlessModeOnOff.equals("true")) {
			firefoxBinary.addCommandLineOptions("--headless");
			firefoxOptions.addArguments("--width=1920");
			firefoxOptions.addArguments("--height=1080");
		}

		firefoxOptions.setBinary(firefoxBinary);

		driver = new FirefoxDriver(firefoxOptions);
		driver.manage().window().maximize();
	}

}
