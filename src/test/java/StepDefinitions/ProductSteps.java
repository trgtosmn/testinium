package StepDefinitions;

import Pages.LoginPage;
import Pages.ProductPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductSteps {

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	@When("User select random product")
	public void userSelectRandomProduct() throws InterruptedException {
		ProductPage productPage = new ProductPage(driver, wait);
		productPage.selectProduct();
		Thread.sleep(3000);
	}

	@When("User click add to basket button")
	public void userClickAddProductToBasketButton() {
		ProductPage productPage = new ProductPage(driver, wait);
		productPage.addProductToBasket();

	}
	@Then("User check product price with after added basket of product price$")
	public void userCheckSecoundPage() throws InterruptedException {
		ProductPage productPage = new ProductPage(driver,wait);
		productPage.assertPriceOfProduct();
	}

	@When("User click increase button")
	public void userClickArttirButton() throws InterruptedException {
		ProductPage productPage = new ProductPage(driver, wait);
		productPage.increaseProduct();
		Thread.sleep(10000);

	}
	@Then("User go to basket and delete product$")
	public void basketControl() throws InterruptedException {
		ProductPage productPage = new ProductPage(driver,wait);
		productPage.goToBasketandDelete();
		Thread.sleep(5000);
	}


}
