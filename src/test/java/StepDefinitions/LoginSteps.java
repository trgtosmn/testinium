package StepDefinitions;

import Pages.HomePage;
import Pages.LoginPage;
import Utilities.PropertiesReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginSteps {

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	public LoginSteps() throws Exception {

		PropertiesReader propertiesReader = new PropertiesReader();
		this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
	}

	@And("^User fills \"([^\"]*)\" to username textbox$")
	public void userFillsEmailTextBox(String email) {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.fillUserName(email);
	}
	@And("^User fills \"([^\"]*)\" to password textbox$")
	public void userFillsPasswordTextBox(String password) {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.fillPassword(password);
	}
	@When("User click login button$")
	public void userClickLoginButton() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.clickGirisYapButton();
	}

	@Then("User succesfully logined$")
	public void userLogined() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.userSuccessfullyLogined();
	}
	@When("^User search \"([^\"]*)\" in search textbox$")
	public void userSearch(String str) {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.search(str);
	}

	@When("User click bul button$")
	public void userClickBulButton() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.clickBulButton();
	}

	@When("User click secound page$")
	public void userClickSecoundButton() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.clickSecoundPage();
	}

	@Then("User check secound page opened$")
	public void userCheckSecoundPage() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.userIsOnSecoundPage();
	}



}