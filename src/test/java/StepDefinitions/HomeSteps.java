package StepDefinitions;

import Pages.HomePage;
import Utilities.BaseClass;
import Utilities.PropertiesReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomeSteps{

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	public HomeSteps() throws Exception {
		PropertiesReader propertiesReader = new PropertiesReader();
		this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
	}

	@Given("^User go to gittigidiyor page")
	public void userClickFancyBoxCloseButton() throws InterruptedException {
		HomePage homePage = new HomePage(driver,wait);
		homePage.navigateToHomePage();
	}

	@When("^User click girisYap button$")
	public void userClickGirisYapCloseButton() throws InterruptedException {
		HomePage homePage = new HomePage(driver,wait);
		homePage.clickGirisYapButton();
	}

	@Then("^User is on homepage$")
	public void userIsOnHomePage() throws InterruptedException {
		HomePage homePage = new HomePage(driver,wait);
		homePage.isOnHomePage();
	}


}
