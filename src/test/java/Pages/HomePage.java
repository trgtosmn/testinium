package Pages;

import Constants.Constants;
import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BaseClass implements Constants {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;

    By girisYap = By.xpath("//div[@title='Giriş Yap']");
    By girisButton = By.xpath("//span[contains(text(),'Giriş Yap')]");

    public HomePage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }
    public void navigateToHomePage(){
        driver.get(URL);
    }

    public void clickGirisYapButton(){
        waitElementClickable(girisYap);
        waitElementClickable(girisButton);
    }

    public void isOnHomePage(){
        currentURL("https://www.gittigidiyor.com/");
    }
}