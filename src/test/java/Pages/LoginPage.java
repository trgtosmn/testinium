package Pages;

import Utilities.BaseClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BaseClass {

	By kullaniciAdi = By.xpath("//input[@id='L-UserNameField']");
	By password = By.xpath("//input[@id='L-PasswordField']");
	By girisYap = By.xpath("//input[@id='gg-login-enter']");
	By myAccount = By.xpath("//div[@class='gekhq4-4 hwMdSM']");
	By searchTextBox = By.xpath("//header/div[3]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[2]/input[1]");
	By bulButton = By.xpath("//span[contains(text(),'BUL')]");
    By secoundPage = By.xpath("//a[contains(text(),'2')]");

	public LoginPage(WebDriver driver, WebDriverWait wait) {

		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void fillUserName(String str) {
		fillElement(kullaniciAdi,str);
	}

	public void fillPassword(String str){
		fillElement(password,str);
	}

	public void clickGirisYapButton(){
		waitElementClickable(girisYap);
	}

	public void userSuccessfullyLogined() throws InterruptedException {
		isDisplayedElements(myAccount);
	}

	public void search(String str){
		fillElement(searchTextBox,str);
	}
	public void clickBulButton(){
		waitElementClickable(bulButton);
	}

	public void clickSecoundPage() throws InterruptedException {
		endOfPage();
		Thread.sleep(10000);
		waitElementClickable(secoundPage);
	}
	public void userIsOnSecoundPage(){
		currentURL("https://www.gittigidiyor.com/arama/?k=bilgisayar&sf=2");


	}
}