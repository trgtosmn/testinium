package Pages;

import Utilities.BaseClass;
import gherkin.lexer.Th;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage extends BaseClass {

	public ProductPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	By products = By.xpath("//li[@product-index='2']");
	By addToBasketBtn = By.xpath("//button[@id='add-to-basket']");
	By basketPrice = By.xpath("//*[@id=\"sp-price-lowPrice\"]");
	By productPrice = By.xpath("//body/div[5]/div[2]/div[1]/div[2]/div[3]/div[2]/ul[1]/li[2]/a[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/p[1]");
	By increase = By.xpath("//*[@id=\"CountSelect\"]/li[3]/a");

	By goToBasket = By.xpath("//*[@id=\"header_wrapper\"]/div[4]/div[3]/a");
	By sil = By.xpath("//body/div[@id='main-content']/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[6]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/a[1]/i[1]");

	public static String priceOfProductList = "0";

	public void goToBasketandDelete() throws InterruptedException {
		Thread.sleep(1500);
		clickElement(goToBasket);
		Thread.sleep(1500);
		clickElement(sil);
	}
	public void selectProduct() throws InterruptedException {
		Thread.sleep(1000);
		String product = getPrice(productPrice);
		this.priceOfProductList = product;
		clickElement(products);

	}

	public void addProductToBasket() {
		endOfPage();
		clickElement(addToBasketBtn);
	}

	public void assertPriceOfProduct(){
		checkPrice(basketPrice,priceOfProductList);

	}
	public void increaseProduct(){
		clickElement(increase);
	}
}
