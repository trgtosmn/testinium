package Utilities;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.refreshed;

public class BaseClass {

	private static WebDriver driver;
	private static WebDriverWait wait;

	public BaseClass(WebDriver driver, WebDriverWait wait) {

		BaseClass.driver = driver;
		BaseClass.wait = wait;
	}

	public void waitElementClickable(By by) {
		wait.until(elementToBeClickable(by)).click();
	}

	public void endOfPage() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(250, document.body.scrollHeight)");
	}

	public void fillElement(By element, String obj) {
		driver.findElement(element).sendKeys(obj);

	}

	public void clickElement(By element) {
		driver.findElement(element).click();
	}

	public void currentURL(String url) {
		Assert.assertEquals(url, driver.getCurrentUrl());
	}

	public void isDisplayedElements(By element) throws InterruptedException {
		Thread.sleep(1000);
		String abc = driver.findElement(element).getText();
		Assert.assertTrue(abc.contains("Hesabım"));
	}

	public void clickToRandom(By by) {
		List<WebElement> images = driver.findElements(by);
		images.get(RandomUtils.nextInt(images.size())).click();
	}

	public void checkPrice(By element1, String element2){
		String first = driver.findElement(element1).getText();
		System.out.println("SEEEPEEET" + first);
		Assert.assertEquals(first,element2);
	}
	public String getPrice(By element){
		String price = driver.findElement(element).getText();
		return price;
	}

}

