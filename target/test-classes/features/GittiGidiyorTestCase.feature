Feature: GittiGidiyor UI case study

  Scenario: The process has been succesfully completed
    Given User go to gittigidiyor page
    Then User is on homepage
    When User click girisYap button
    And User fills "alperglc38@gmail.com" to username textbox
    And User fills "199410ag" to password textbox
    When User click login button
    Then User succesfully logined
    When User search "bilgisayar" in search textbox
    When User click bul button
    When User click secound page
    Then User check secound page opened
    When User select random product
    When User click add to basket button
    Then User check ürün fiyatı ile sepete eklendikten sonraki fiyatı karşılaştırır
    When User click arttir button
    Then User sepete gider ve ürünü siler