$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("GittiGidiyorTestCase.feature");
formatter.feature({
  "line": 1,
  "name": "GittiGidiyor UI case study",
  "description": "",
  "id": "gittigidiyor-uı-case-study",
  "keyword": "Feature"
});
formatter.before({
  "duration": 6927450200,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "The process has been succesfully completed",
  "description": "",
  "id": "gittigidiyor-uı-case-study;the-process-has-been-succesfully-completed",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User go to gittigidiyor page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User is on homepage",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User click girisYap button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "User fills \"alperglc38@gmail.com\" to username textbox",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User fills \"199410ag\" to password textbox",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "User click login button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User succesfully logined",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User search \"bilgisayar\" in search textbox",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User click bul button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "User click secound page",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "User check secound page opened",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "User select random product",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User click add to basket button",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "User check ürün fiyatı ile sepete eklendikten sonraki fiyatı karşılaştırır",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "User click arttir button",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "User sepete gider ve ürünü siler",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.userClickFancyBoxCloseButton()"
});
formatter.result({
  "duration": 2137655300,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.userIsOnHomePage()"
});
formatter.result({
  "duration": 17108800,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.userClickGirisYapCloseButton()"
});
formatter.result({
  "duration": 2322209600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alperglc38@gmail.com",
      "offset": 12
    }
  ],
  "location": "LoginSteps.userFillsEmailTextBox(String)"
});
formatter.result({
  "duration": 110906800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "199410ag",
      "offset": 12
    }
  ],
  "location": "LoginSteps.userFillsPasswordTextBox(String)"
});
formatter.result({
  "duration": 123141100,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userClickLoginButton()"
});
formatter.result({
  "duration": 2551131900,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userLogined()"
});
formatter.result({
  "duration": 1078079200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "bilgisayar",
      "offset": 13
    }
  ],
  "location": "LoginSteps.userSearch(String)"
});
formatter.result({
  "duration": 282845800,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userClickBulButton()"
});
formatter.result({
  "duration": 4127100200,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userClickSecoundButton()"
});
formatter.result({
  "duration": 12846307800,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userCheckSecoundPage()"
});
formatter.result({
  "duration": 36778500,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userSelectRandomProduct()"
});
formatter.result({
  "duration": 5866246800,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userClickAddProductToBasketButton()"
});
formatter.result({
  "duration": 451123700,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userCheckSecoundPage()"
});
formatter.result({
  "duration": 67288300,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userClickArttirButton()"
});
formatter.result({
  "duration": 10134815100,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.sepetKontorlü()"
});
formatter.result({
  "duration": 9015654500,
  "status": "passed"
});
formatter.after({
  "duration": 774447600,
  "status": "passed"
});
});